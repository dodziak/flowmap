import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MarkerModel } from './model/markermodel';
import { MapService } from './services/map.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  showFlowLegend: boolean = false;
  showAreaLegend: boolean = false;
  isLocationChosen: boolean = false;

  latitude: number = 54.66111163864411;
  longitude: number = 17.961796641675388;

  constructor(public mapService: MapService) { }

  onMapClick(event) {
    const markerObj = {
      lat: event.coords.lat,
      lng: event.coords.lng,
      name: this.mapService.name,
      type: this.mapService.type,
      flowValue: this.mapService.flowValue,
    } as MarkerModel;

    this.mapService.markers.push(markerObj);

    this.isLocationChosen = true;

    console.log(`Lat:${this.latitude}`);
    console.log(`Long:${this.longitude}`);
    console.log(event);
  }

  iconColorChange(marker): string {
    switch (marker.type) {
      case 'Przepływ nieznany':
        return 'http://labs.google.com/ridefinder/images/mm_20_gray.png';
      case 'Przepływ - SUW/ZUW':
        return 'http://labs.google.com/ridefinder/images/mm_20_green.png';
      case 'Przepływ - zbiornik':
        return 'http://labs.google.com/ridefinder/images/mm_20_orange.png';
      case 'Przepływ międzystrefowy':
        return 'http://labs.google.com/ridefinder/images/mm_20_blue.png';
      case 'Sprzedaż - Online':
        return 'http://labs.google.com/ridefinder/images/mm_20_yellow.png';
      case 'Sprzedaż - odczyt co 12h':
        return 'http://labs.google.com/ridefinder/images/mm_20_purple.png';
      case 'Sprzedaż- odczyt ręczny':
        return 'http://labs.google.com/ridefinder/images/mm_20_brown.png';
      case 'Przepływ wirtualny':
        return 'http://labs.google.com/ridefinder/images/mm_20_black.png';
    }
  }

}
