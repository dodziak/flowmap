export interface MarkerModel {
    lat: number;
    lng: number;

    name?: string;
    type?: string;
    flowValue?: number;
}