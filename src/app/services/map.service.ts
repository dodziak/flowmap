import { Injectable } from '@angular/core';
import { MarkerModel } from '../model/markermodel';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  formShow: boolean = false;

  name: string;
  flowValue: number;
  type: string;

  markers: MarkerModel[] = [];

  flowArr = [
    { id: 1, name: 'Przepływ nieznany' },
    { id: 2, name: 'Przepływ - SUW/ZUW' },
    { id: 3, name: 'Przepływ - zbiornik' },
    { id: 4, name: 'Przepływ międzystrefowy' },
    { id: 5, name: 'Sprzedaż - Online' },
    { id: 6, name: 'Sprzedaż - odczyt co 12h' },
    { id: 7, name: 'Sprzedaż- odczyt ręczny' },
    { id: 8, name: 'Przepływ wirtualny' },
  ];

  constructor() { }
}
