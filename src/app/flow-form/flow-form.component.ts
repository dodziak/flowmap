import { MapService } from './../services/map.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-flow-form',
  templateUrl: './flow-form.component.html',
  styleUrls: ['./flow-form.component.scss']
})
export class FlowFormComponent {

  flowForm: FormGroup;

  get f() {
    return this.flowForm.controls;
  }

  constructor(public mapService: MapService, private formBuilder: FormBuilder) {
    this.flowForm = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      flowValue: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]\d*$/), Validators.min(1)]),
      type: new FormControl('', Validators.required),
    });
  }

  onSubmit(flowData) {
    this.mapService.name = flowData.name;
    this.mapService.type = flowData.type;
    this.mapService.flowValue = flowData.flowValue;

    console.log({ flowData });
  }

  clearForm() {
    this.flowForm.reset();
  }

}
